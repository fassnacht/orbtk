/// The `ScrollMode` defines the mode of a scroll direction.
#[derive(Copy, Clone, PartialEq)]
pub enum ScrollMode {
    /// Scrolling will process by `ScrollViewer` logic
    Auto,

    /// Scrolling could be handled from outside. It will not be process by `ScrollViewer` logic.
    None,
}

impl Default for ScrollMode {
    fn default() -> Self {
        ScrollMode::Auto
    }
}