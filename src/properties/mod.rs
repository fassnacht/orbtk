//! This module contains non visual structures like point, rectangle, color and thickness.

pub use orbclient::color::Color;
pub use orbclient::Renderer as OrbRenderer;
pub use orbgl::Canvas;
pub use orbimage::Image;

pub use self::bounds::Bounds;
pub use self::constraint::Constraint;
pub use self::enabled::Enabled;
pub use self::focused::Focused;
pub use self::font_icon::{FontIcon, PrimaryFontIcon, SecondaryFontIcon};
pub use self::label::Label;
pub use self::mouse_over::MouseOver;
pub use self::offset::Offset;
pub use self::padding::Padding;
pub use self::point::Point;
pub use self::pressed::Pressed;
pub use self::scroll_viewer_mode::ScrollViewerMode;
pub use self::selected::Selected;
pub use self::text_selection::TextSelection;
pub use self::water_mark::WaterMark;

mod bounds;
mod constraint;
mod enabled;
mod focused;
mod font_icon;
mod label;
mod mouse_over;
mod offset;
mod padding;
mod point;
mod pressed;
mod scroll_viewer_mode;
mod selected;
mod text_selection;
mod water_mark;

#[cfg(test)]
mod tests;

// pub use self::color::Color;
// pub mod color;
